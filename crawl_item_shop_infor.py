from scraper.driver import Scraper
from utils.get_all_categories import get_all_categories
from settings.config import Setting
from concurrent.futures import ThreadPoolExecutor

def crawl_item_shop_infor(catid_and_parent_catid):
    print(catid_and_parent_catid)
    scraper = Scraper()
    scraper.login(url="https://shopee.vn/buyer/login?next=https%3A%2F%2Fshopee.vn%2F")
    scraper.get_product_urls(catid_and_parent_catid=catid_and_parent_catid)

if __name__=="__main__":
    
    MAX_WORKERS = Setting.MAX_WORKERS
    
    list_catid_parent_catid = get_all_categories()
    print("list_catid_parent_catid: ", list_catid_parent_catid)
    
    with ThreadPoolExecutor(max_workers=MAX_WORKERS) as executor:
        executor.map(crawl_item_shop_infor, list_catid_parent_catid)
    
    
    