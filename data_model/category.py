from typing import Union
from pydantic import BaseModel

class Category(BaseModel):
    catid: int
    parent_catid: int
    name: str
    display_name: str
    image: str
    unselected_image: str
    selected_image: str
    level: int
