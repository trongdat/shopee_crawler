from selenium.webdriver.common.by import By
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager

import requests
from random import randint
import time
import sys
import os
config_dir = os.path.join('../', '')
sys.path.insert(0, config_dir)
from settings.config import Env, Setting
from utils.get_all_categories import get_all_categories

from database.database import Database


ITEM_SHOP_TB = Setting.ITEM_SHOP_TB

class Scraper:
    def __init__(self):
        save_path = "../data/product_urls"
        if not os.path.exists(save_path):
            os.makedirs(save_path)
            print('[INFO] creating product_urls folder')
        self.save_path = save_path

        if Setting.BROWSER == Env.FIREFOX_DRIVER:
            self.driver = webdriver.Firefox()
        else:
            chrome_options = webdriver.ChromeOptions()
            # chrome_options.add_argument("--incognito")
            chrome_options.add_argument("--window-size=1920x1080")
            service = Service(executable_path="/usr/bin/chromedriver")
            self.driver = webdriver.Chrome(service=service, options=chrome_options)

            # options = Options()
            # options.add_argument('--headless')
            # options.add_argument("--incognito")
            # options.add_argument("--window-size=1920x1080")
            # self.driver = webdriver.Chrome("/usr/bin/chromedriver", options=options)

    def test_driver(self, url):
        self.driver.get(url)

    def login(self, url): #url: https://shopee.vn/buyer/login?next=https%3A%2F%2Fshopee.vn%2F
        self.driver.get(url)

        wait = WebDriverWait(self.driver, randint(30, 40))
        wait.until(EC.element_to_be_clickable((By.ID, 'loginKey'))).send_keys("")
        wait.until(EC.element_to_be_clickable((By.ID, 'password'))).send_keys("")
        wait.until(EC.element_to_be_clickable((By.XPATH, "//button[text()='Đăng nhập']"))).click()

        # wait = WebDriverWait(self.driver, randint(30, 40))
        # wait.until(EC.element_to_be_clickable((By.NAME, 'loginKey'))).send_keys("0976358763")
        # wait.until(EC.element_to_be_clickable((By.NAME, 'password'))).send_keys("Hero@123")
        # # Below line will click on LOG IN button
        # wait.until(EC.element_to_be_clickable((By.XPATH, "//button[text()='Đăng nhập']"))).click()

        return self.driver
    
    def get_id(self, product_url):
        itemid = int(product_url.split(".")[-1].split("?")[0])
        shopid = int(product_url.split(".")[-2])
        return itemid, shopid
    
    def get_product_urls(self, catid_and_parent_catid):
        catid = catid_and_parent_catid[0]
        parent_catid = catid_and_parent_catid[1]
        cat_url = f"https://shopee.vn/i-cat.{catid_and_parent_catid[1]}.{catid_and_parent_catid[0]}"
        print(f"[INFO] Gathering product links from {cat_url}")
        self.driver.get(cat_url)

        try:
            time.sleep(2)
            number_of_page_element = int(self.driver.find_element(By.CSS_SELECTOR, 'span.shopee-mini-page-controller__total'))
            number_of_page = number_of_page_element.get_attribute('textContent')
            print("In try number_of_page: ", number_of_page)
        except Exception as ex:
            print(f'[ERROR] {ex}')
            number_of_page = 9          
            print('[INFO] Number of page: ', number_of_page)
            
        for i in range(0, number_of_page):
            print(f"Page: {i} 'of' {number_of_page}")
            try:
                time.sleep(2)
                btn_next = self.driver.find_element(By.CSS_SELECTOR, \
                            'a.shopee-button-outline.shopee-mini-page-controller__next-btn')
                self.driver.execute_script("arguments[0].scrollIntoView();", btn_next)
                time.sleep(2)
            except Exception as ex:
                print(ex)
            
            #Get product infor
            list_product = self.driver.find_elements(By.CSS_SELECTOR, \
                            'ul.row.shopee-search-item-result__items > li')
            for product in list_product:
                try:
                    #scroll to element
                    self.driver.execute_script("arguments[0].scrollIntoView();", product)
                    product = product.find_element(By.CSS_SELECTOR, 'a')
                    product_url = product.get_attribute('href')
                    itemid, shopid = self.get_id(product_url)
                    item_shop_url = f"https://shopee.vn/api/v4/pdp/hot_sales/get?\
                        itemid={itemid}&limit=8&offset=0&shopid={shopid}"
                    values = (catid, parent_catid, cat_url, shopid, itemid, item_shop_url)
                    Database.insert(table=ITEM_SHOP_TB, values=values) #insert to item_shop table
                except Exception as ex:
                    print(f'[ERROR] {ex}')
            # next page
            try:
                try:
                    btn_next.click()
                except:
                    self.driver.find_element(By.CSS_SELECTOR, \
                        'a.shopee-button-outline.shopee-mini-page-controller__next-btn')\
                        .click()
            except Exception as ex:
                print(f'[ERROR] {ex}')
                continue

        self.driver.quit()
        return 0



if __name__ == '__main__':
    urls = []
    list_catid_parent_catid = get_all_categories()

    bot = Scraper()

    # bot.test_driver(url="https://www.google.com/")
    # bot.login(url="https://shopee.vn/buyer/login?next=https%3A%2F%2Fshopee.vn%2F")

    for catid_and_parent_catid in list_catid_parent_catid: #catid_and_parent_catid: (11116486, 11116484)
        product_urls = bot.get_product_urls(catid_and_parent_catid)
        print("***************************************************************************************")
        print(product_urls)


    