import sys
import os
config_dir = os.path.join('../', '')
sys.path.insert(0, config_dir)
from database.database import Database
from settings.config import Setting
import json

PARENT_TB=Setting.PARENT_TB
CHILDREN_TB=Setting.CHILDREN_TB

def json_to_data_insert(json_data):
    all_values = (int(json_data["catid"]),
                  int(json_data["parent_catid"]),
                  str(json_data["name"]),
                  str(json_data["display_name"]), 
                  str(json_data["image"]), 
                  str(json_data["unselected_image"]), 
                  str(json_data["selected_image"]),
                  int(json_data["level"]))
    return all_values

def import_data(json_path):
    with open(json_path) as f:
        json_data = json.load(f)
    
    for parent_category in json_data["data"]["category_list"]:
        all_values_p = json_to_data_insert(parent_category)
        res_insert_p = Database.insert(table=PARENT_TB, values=all_values_p)
        print("Insert Parent Category Values: ", all_values_p)
        if not res_insert_p:
            print("****************Insert Parent Category: False****************")
        for children_category in parent_category["children"]:
            all_values_c = json_to_data_insert(children_category)
            print(" "*5, "Insert Children Category Values: ", all_values_c)
            res_insert_c = Database.insert(table=CHILDREN_TB, values=all_values_c)
            if not res_insert_c:
                print(" "*5,"*************Insert Children Category: False*************")
            print(" "*5, "=============================================")
        print("============================================================================================")


if __name__=="__main__":
    json_path = "../data/shopee/category.json"
    import_data(json_path=json_path)
