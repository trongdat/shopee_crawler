from database.database import Database
from settings.config import Setting


def get_all_categories():
    CHILDREN_TB = Setting.CHILDREN_TB

    list_catid_parent_catid = Database.get_all_records(table=CHILDREN_TB, 
                                           columns=("catid", "parent_catid"))
    return list_catid_parent_catid #[(11116488, 11116484), (11116486, 11116484)]
