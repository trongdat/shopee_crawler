import sys
import os
config_dir = os.path.join('../', '')
sys.path.insert(0, config_dir)
from database.database import Database
from settings.config import Setting

import pandas as pd
import random
import time

PRODUCT_TB=Setting.PRODUCT_TB

def row_df_to_values(row):
    shopid = int(row["url"].split(".")[-2])
    itemid = int(row["url"].split(".")[-1])

    values = (str(row["product_name"]).replace("\'", "\""),
                shopid,
                itemid,
                str(row["url"]), 
                round(random.uniform(1.1, 4.9), 1), 
                int(row["product_price"]), 
                int(row["product_revenue"]),
                int(time.time()*1000))
    return values

def insert_data(csv_path):
    df = pd.read_csv(csv_path, lineterminator='\n')
    len_df = len(df.index)
    for i in range(0, len_df):
        values = row_df_to_values(row=df.iloc[i])
        res_insert = Database.insert(table=PRODUCT_TB, values=values)
        if not res_insert:
            print(f"ROW {i}: ", values)
            print("Rest Insert: ", res_insert)
            print("============================================================")
    


if __name__=="__main__":
    csv_path = "../data/shopee/products.csv"
    insert_data(csv_path=csv_path)
