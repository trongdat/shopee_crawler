import asyncio
import aiohttp
import time
from settings.config import Setting
from database.database import Database
import math
from itertools import cycle , islice


ITEM_SHOP_TB = Setting.ITEM_SHOP_TB
PRODUCT_TB= Setting.PRODUCT_TB
MAX_WORKERS = Setting.MAX_WORKERS


async def get_all_item_shop():
    print("Get all item shop")
    all_itemid_shopid = Database.get_all_records(table=ITEM_SHOP_TB, 
                                                   columns=("itemid", "shopid"))
    return all_itemid_shopid

async def set_url(itemid, shopid):
    url = f"https://shopee.vn/api/v4/pdp/hot_sales/get?\
        itemid={itemid}&limit=8&offset=0&shopid={shopid}"
    return url

async def fetch(url, session):
    async with session.get(url) as response:
        if response.status_code != 200:
            print(f'[ERROR] Response status {response.status_code}')
            return None
        result = response.json()
        if not result.get('data') or not result['data'].get('items'):
            print("[ERROR] Doesn't exist product list")
            return None
        # Get detail
        data = []
        products = result['data'].get('items')
        for product in products:
            item = {}
            item['name'] = product['name']
            item['url'] = f"https://shopee.vn/a-i.{product['shopid']}.{product['itemid']}"
            item['rating'] = product['rating']
            item['price'] = product['price_max']/10000
            item['revenue'] = product['historical_sold']*item['product_price']
            data.append(item)
        return data

async def crawl_data(itemid_shopid, session):
    itemid = itemid_shopid[0]
    shopid = itemid_shopid[1]
    url = await set_url(itemid=itemid, shopid=shopid)
    print(f"Crawling Data: {url}")
    data = await fetch(url, session)

    #Insert to product table
    for item in data:
        create_time = int(time.time()*1000)
        values = (item["name"], shopid, itemid, item['url'], item['rating'], 
                  item['price'], item['revenue'], create_time)
        res_insert = Database.insert(table=PRODUCT_TB, values=values)
        print("********res_insert*********", res_insert)

async def main(): 
    all_itemid_shopid = await get_all_item_shop()
    i = cycle(all_itemid_shopid)
    slc = MAX_WORKERS
    async with aiohttp.ClientSession() as session:
        for _ in range(math.ceil(len(all_itemid_shopid)/slc)):
            slc_urls = (list(islice(i,slc)))
            tasks = [crawl_data(itemid_shopid, session) for itemid_shopid in slc_urls]
            await asyncio.gather(*tasks)

if __name__ == "__main__":
    asyncio.run(main())
