from pydantic_settings import BaseSettings
from enum import Enum


class Env:
    CHROME_DRIVER = "Chrome"
    FIREFOX_DRIVER = "Firefox"

class Setting(BaseSettings):
    # Pydantic will read the environment variables in a case-insensitive way, to set values for below
    BROWSER: str
    CATEGORY_URL: str

    MAX_WORKERS: int
    
    #All enviroment variables
    POSTGRES_HOST: str
    POSTGRES_PORT: int
    POSTGRES_USER: str
    POSTGRES_PASSWORD: str
    POSTGRES_DB: str
    PARENT_TB: str
    CHILDREN_TB: str
    PRODUCT_TB: str
    ITEM_SHOP_TB: str

    # class Config:
    #     env_file : str "dev.env"    


Setting = Setting()
Env = Env()
# print(Setting)
# print(Env)

