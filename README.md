# Crawl Data from Shopee ecommerce

# System

* Data Crawler System

![Data Crawler System](./data/data_system/data_crawler.png)

* Flow System

![Flow System](./data/data_system/Flow_system.png)

* Crawl Category Infor(`crawl_category_infor.py`)
* Crawl Item Shop Infor(`crawl_item_shop_infor.py`)
* Crawl Product Infor(`async_crawl_product_infor.py`)
* Export data to csv (`handle_data/stream_data_in_chunks_to_csv.py`)

## RUN

* `.env` file: All environment parameters
* Deploy PostgreSQL Database
  ``docker compose -f docker-compose.yml up -d``
* Deploy Spark Cluster
  ``docker compose -f docker-compose-spark-cluster.yml up -d``
* Start Crawl Data

```
conda create --name shopee_crwaler python=3.11
conda activate shopee_crwaler
pip install -r _install/requirements.txt
```

* Crawl Category Infor - Crawl Item Shop Infor - Crawl Product Infor

```
python crawl_category_infor.py`
python crawl_item_shop_infor.py`
python async_crawl_product_infor.py
```

* Export data to csv, Output: data/shopee_mart/products.csv

```
cd handle_data
python stream_data_in_chunks_to_csv.py
```

## Result:

MAX_WORKERS=3

* Number of products: **134,146**
* Crawl Data Speep: **...**
* Transform Data Speed: **24.613.945 product/min**
