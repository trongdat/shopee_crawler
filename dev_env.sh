export CATEGORY_URL="https://shopee.vn/api/v4/pages/get_category_tree"
export BROWSER=Chrome

export MAX_WORKERS=16


# Export variables for PostgreSQL
export POSTGRES_HOST=localhost
export POSTGRES_PORT=5432
export POSTGRES_USER=postgres_user
export POSTGRES_PASSWORD=123456aA@
export POSTGRES_DB=datalake
export PARENT_TB=parent_category
export CHILDREN_TB=children_category
export PRODUCT_TB=product
export ITEM_SHOP_TB=item_shop

