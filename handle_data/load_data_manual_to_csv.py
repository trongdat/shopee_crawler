import psycopg2
import time
import pandas as pd




connection = psycopg2.connect(
                database="datalake",
                user="postgres_user",
                password="123456aA@",
                host="localhost",
                port=5432)

start = time.time()
query = """SELECT * FROM product"""
cursor = connection.cursor()
cursor.execute(query)
values = cursor.fetchall()
cursor.close()

# df =pd.DataFrame(values)
# df.to_csv('../data/shopee_mart/products.csv', index=False)

df =pd.DataFrame(values)
df.to_excel('../data/shopee_mart/products.xlsx', index=False)

end = time.time()
total_time = int((end -start)*1000)
print("total time: ", total_time)