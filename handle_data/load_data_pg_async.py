import asyncio
import asyncpg
import pandas 
import time

start = time.time()

async def run():
    conn = await asyncpg.connect(user='postgres_user', password='123456aA@',
                                 database='datalake', host='localhost')
    query = """SELECT * FROM product"""
    values = await conn.fetch(query)

    # print(values[0])
    # print(type(values[0]))
    await conn.close()
    return values

loop = asyncio.get_event_loop()
values = loop.run_until_complete(run())

end = time.time()
total_time = int((end -start)*1000)
print("total time: ", total_time)
