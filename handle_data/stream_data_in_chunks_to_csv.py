import psycopg2
import time


conn = psycopg2.connect(
                database="datalake",
                user="postgres_user",
                password="123456aA@",
                host="localhost",
                port=5432)

cur = conn.cursor()

query = """SELECT * FROM product"""
start = time.time()

#https://dev.to/varungujarathi9/exporting-a-large-postgresql-table-using-python-4non
with open('../data/shopee_mart/products.csv', 'w') as f:
  cur.copy_expert("COPY ({}) TO STDOUT WITH CSV HEADER".format(query), f)
print("Query results exported to CSV!")

end = time.time()
total_time = int((end -start)*1000)
print("total time: ", total_time)