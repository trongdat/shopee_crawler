
CREATE TABLE IF NOT EXISTS parent_category(
    catid INT PRIMARY KEY,
    parent_catid INT DEFAULT 0,
    name VARCHAR,
    display_name VARCHAR,
    image VARCHAR,
    unselected_image VARCHAR,
    selected_image VARCHAR,
    level INT DEFAULT 1
    );

CREATE TABLE IF NOT EXISTS children_category(
    catid INT PRIMARY KEY,
    parent_catid INT REFERENCES parent_category(catid),
    name VARCHAR,
    display_name VARCHAR,
    image VARCHAR,
    unselected_image VARCHAR,
    selected_image VARCHAR,
    level INT DEFAULT 2
    );

CREATE TABLE IF NOT EXISTS item_shop(
    catid INT ,
    parent_catid INT ,
    cat_url VARCHAR,
    shopid BIGINT,
    itemid BIGINT,
    item_url VARCHAR
    );
    
CREATE TABLE IF NOT EXISTS product(
    name VARCHAR,
    shopid BIGINT,
    itemid BIGINT,
    url VARCHAR,
    rating FLOAT(4),
    price INT,
    revenue BIGINT,
    create_time BIGINT
    );


