import pyspark 
from pyspark.sql import SparkSession 

# Set the master URL to connect to the Spark cluster
master_url = "spark://localhost:17077"

# Create a Spark session
spark = SparkSession.builder \
    .config("spark.jars", "/usr/local/postgresql-42.2.5.jar")\
    .appName("PythonSparkExample") \
    .master(master_url) \
    .getOrCreate()
    #.master(master_url) \

df = spark.read.format("jdbc").option("url", "jdbc:postgresql://localhost:5432/datalake")\
                            .option("driver", "org.postgresql.Driver")\
                            .option("dbtable", "parent_category")\
                            .option("user", "postgres_user")\
                            .option("password", "123456aA@")\
                            .load()

df.write.csv("test.csv")

print(df)

df.printSchema()

# Show the DataFrame
df.show()

# Stop the Spark session
spark.stop()

