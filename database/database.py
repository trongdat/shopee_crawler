import psycopg2
from psycopg2 import IntegrityError
from settings.config import Setting


class MyPostgreSQL:
    def __init__(self, database, user, password, host, port=5432):
        self.database = database
        self.connection = None
        try:
            self.connection = psycopg2.connect(
                                database=database,
                                user=user,
                                password=password,
                                host=host,
                                port=port)
            print("Database connected")
        except:
            print("Database not connected")
            raise Exception("Database not connected")
            
        if self.connection is not None:
            self.connection.autocommit = True
        #     cursor = self.connection.cursor()
        #     cursor.execute("SELECT datname FROM pg_database")
        #     list_database = cursor.fetchall()
        #     print("list_database: ", list_database)

    def close(self):
        self.connection.close()
        print('Close connection')

    def execute_query(self, query):
        cursor = self.connection.cursor()
        cursor.execute(query)
        cursor.close()

    def get_one_record(self, table, columns, column, condition):
        try:
            # Constructing the parameterized query
            query = f"SELECT {', '.join(columns)} FROM {table} WHERE {column} = '{condition}';"
            # print(query)
            cursor = self.connection.cursor()
            cursor.execute(query)
            row = cursor.fetchone()
            cursor.close()

            if row:
                # Convert the result to a dictionary
                result_dict = dict(zip(columns, row))
                return result_dict
            else:
                return None
        except Exception as e:
            print(f"Error retrieving record: {e}")
            return None
        
    def get_all_records(self, table, columns):
        try:
            # Constructing the parameterized query
            query = f"SELECT {', '.join(columns)} FROM {table};"
            # print(query)
            cursor = self.connection.cursor()
            cursor.execute(query)
            rows = cursor.fetchall()
            cursor.close()
            return rows
        except Exception as e:
            print(f"Error retrieving record: {e}")
            return None

    def check_exists(self, table, column, condition):
        query = f"SELECT 1 FROM {table} WHERE {column} = '{condition}'"
        cursor = self.connection.cursor()
        cursor.execute(query)
        row = cursor.fetchone()
        if not row:
            cursor.close()
            return False
        else:
            cursor.close()
            return True
        
    def insert(self, table=None,  values=None, column = None):
        try:
            if column != None:
                query = f'INSERT INTO {table} ('
                for item in column:
                    query += "\""+item+"\","
                query = query[:len(query)-1] + ')'
                query = f'{query} VALUES {values}'
            else:
                query = f'INSERT INTO {table} VALUES {values};'
            # print(query)
            cursor = self.connection.cursor()
            cursor.execute(query)   
            cursor.close()
            self.connection.commit()
            return True
        except Exception as err:
            print("Exception: ", err)
            return False

    def update(self, table,  column_set, condition_set, column, condition):
        query = f"UPDATE {table} SET {column_set} = '{condition_set}' WHERE {column} = '{condition}';"
        # print(query)
        cursor = self.connection.cursor()
        cursor.execute(query) 
        affected_rows = cursor.rowcount  
        self.connection.commit()
        cursor.close()
        return affected_rows


    def delete(self, table, condition):
        query = f"DELETE FROM {table} WHERE ai_model_id = '{condition}';"
        # print(query)
        cursor = self.connection.cursor()
        cursor.execute(query) 
        affected_rows = cursor.rowcount  
        self.connection.commit()
        cursor.close()
        return affected_rows
        
Database = MyPostgreSQL(database=Setting.POSTGRES_DB, user=Setting.POSTGRES_USER, 
                        password=Setting.POSTGRES_PASSWORD, host=Setting.POSTGRES_HOST, 
                        port=Setting.POSTGRES_PORT)



